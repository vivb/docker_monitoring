#!/bin/bash

export ELK_TAG="6.3.0"
ERROR_MSG="Please chose one of the two modes: \\n a) Unsecure: sh setup.sh unsecure \\n b) Secure: sh setup.sh secure"

if [ $# -eq 0 ]; then
  echo "$ERROR_MSG"

elif [ $# -eq 1 ]; then
  if [ "$1" = "unsecure" ]; then
    echo "------------------------------------------------------------"
    echo "############################### Starting monitoring and logging container groups..."
    echo "------------------------------------------------------------"
    docker-compose -f monitoring/docker-compose.unsecure.yml up -d
    docker-compose -f logging/docker-compose.unsecure.yml up -d

    echo "------------------------------------------------------------"
    echo "############################### Output from 'docker ps'..."
    echo "------------------------------------------------------------"
    docker ps

   else
    echo "$ERROR_MSG"
  fi

elif [ $# -eq 3 ]; then
  if [ "$1" = "secure" ]; then
    echo "------------------------------------------------------------"
    echo "############################### Starting monitoring and logging container groups..."
    echo "------------------------------------------------------------"
    docker-compose -f monitoring/docker-compose.secure.yml up -d
    docker-compose -f logging/docker-compose.secure.yml up -d


    echo "------------------------------------------------------------"
    echo "###############################  Waiting 10 seconds for the monitoring and logging container groups to settle in before starting proxy containers..."
    echo "------------------------------------------------------------"
    sleep 10

    echo "------------------------------------------------------------"
    echo "############################### Restarting proxy container group..."
    echo "------------------------------------------------------------"
    docker-compose -f proxy/docker-compose.yml up -d

    echo "------------------------------------------------------------"
    echo "############################### Output from 'docker ps'..."
    echo "------------------------------------------------------------"
    docker ps

  else
    echo "$ERROR_MSG"
  fi

else
  echo "$ERROR_MSG"
fi
